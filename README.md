# HecateAuthenticator

* Permite autenticarse contra la base de datos de **Templo de Hécate** 8.0 mediante servicios.
* Permite autenticarse contra la base de datos de **Identidad** mediantes servicios.

Para usarlo añadir en `composer.json` el siguiente repositorio:

```
"repositories" : [{
        "type" : "vcs",
        "url" : "git@bitbucket.org:kether/hecateauthenticator.git"
    }]
```

Y luego bajo el apartado `require`:

```
"estudiohecate/hecate-authenticator" : "^2.0",
```