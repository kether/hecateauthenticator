<?php

namespace EstudioHecate\Bundle\HecateAuthenticator\Security;

use EstudioHecate\Bundle\HecateAuthenticator\Entity\Usuario;
use GuzzleHttp\Exception\RequestException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;
    
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    /**
     * @var RouterInterface
     */
    private $router;
    
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;
    
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, string $loginTdhUrl)
    {
        $this->entityManager    = $entityManager;
        $this->router           = $router;
        $this->guzzleClient     = new \GuzzleHttp\Client(['base_uri' => $loginTdhUrl]);
        
    }
    
    public function supports(Request $request)
    {
        return $this->getRouteLogin() === $request->attributes->get('_route') && $request->isMethod('POST');
    }
    
    public function getCredentials(Request $request)
    {
        if($form = $request->request->get('form')) {
            $credentials = [
                'usuario' => $form['_username'],
                'clave' => $form['_password'],
                'csrf_token' => $form['_token']
            ];
        } else {
            $credentials = [
                'usuario' => $request->request->get('_username'),
                'clave' => $request->request->get('_password'),
                'csrf_token' => $request->request->get('_token')
            ];
        }
        
        if(empty($credentials)) {
            throw new CustomUserMessageAuthenticationException('No se encontraron los datos de los campos del formulario de login');
        }
        
        $request->getSession()->set(Security::LAST_USERNAME, $credentials['usuario']);
        
        return $credentials;
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $r = $this->guzzleClient->post('login', ['json' => ['usuario' => $credentials['usuario'], 'clave' => $credentials['clave']]]);
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $r = $e->getResponse();
            }
        }
        
        $user = null;
        
        if(isset($r) && $r->getStatusCode() == 200) {
            $jsonRaw = json_decode($r->getBody());
            $apiKey = $jsonRaw->apikey;
            
            if(isset($jsonRaw->data)) {
                $json = $jsonRaw->data;
            } else {
                $json = json_decode($this->guzzleClient->post('user_info', ['json' => [], 'headers' => ['apikey' => $apiKey]])->getBody());
            }
            
            try {
                if(!($user = $this->entityManager->getRepository($this->getUserClass())->loadByCredential($credentials['usuario']))) {
                    $className = $userProvider->getClassName();
                    $user = new $className();
                }
            } catch (UsernameNotFoundException $e) {
                $className = $userProvider->getClassName();
                $user = new $className();
            }
            
            $user->setApikey($apiKey)
                ->setUsuario($json->usuario)
                ->setIsAdmin($json->super)
                ->setSeudonimo($json->seudonimo)
                ->setEmail($json->email);
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        
        if (!$user) {
            throw new CustomUserMessageAuthenticationException('No encontramos al usuario.');
        }
        
        return $user;
    }
    
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }
        
        return new RedirectResponse($this->router->generate($this->getRouteRedirectOnSuccess()));
    }
    
    protected function getUserClass()
    {
        return Usuario::class;
    }
    
    protected function getRouteRedirectOnSuccess() : string
    {
        return 'homepage';
    }
    
    protected function getRouteLogin() : string
    {
        return 'login';
    }
    
    /**
     * {@inheritDoc}
     * @see \Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator::getLoginUrl()
     */
    protected function getLoginUrl() : string
    {
        return $this->router->generate($this->getRouteLogin());
    }
}
