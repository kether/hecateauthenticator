<?php

namespace EstudioHecate\Bundle\HecateAuthenticator\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Usuario
 *
 * @ORM\MappedSuperclass
 */
abstract class Usuario implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=255, unique=true)
     */
    protected $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="seudonimo", type="string", length=255)
     */
    protected $seudonimo;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=255, unique=true)
     */
    protected $apikey;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean")
     */
    protected $isAdmin = false;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="creado", type="datetime")
     */
    protected $creado;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="actualizado", type="datetime")
     */
    protected $actualizado;

    public function __toString()
    {
        return $this->getUsuario();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUsuario(?string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setSeudonimo(?string $seudonimo): self
    {
        $this->seudonimo = $seudonimo;

        return $this;
    }

    public function getSeudonimo(): ?string
    {
        return $this->seudonimo;
    }

    public function setApikey(?string $apikey): self
    {
        $this->apikey = $apikey;

        return $this;
    }

    public function getApikey(): ?string
    {
        return $this->apikey;
    }

    public function setEmail(string $email): ?self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setCreado(?\DateTimeInterface $creado): self
    {
        $this->creado = $creado;

        return $this;
    }

    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    public function setActualizado(\DateTimeInterface $actualizado): self
    {
        $this->actualizado = $actualizado;

        return $this;
    }

    public function getActualizado(): ?\DateTimeInterface
    {
        return $this->actualizado;
    }

    public function setIsAdmin(bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function getRoles(): array
    {
        if($this->getIsAdmin()) {
            return ['ROLE_SUPER_ADMIN'];
        } else {
            return ['ROLE_USER'];
        }
    }

    public function getUsername(): ?string
    {
        return $this->getUsuario();
    }

    public function getPassword(): string
    {
        return sha1($this->getUsuario().$this->getId());
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // @todo
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSave()
    {
        if(is_null($this->apikey)) {
            $this->apikey = '';
        }

        if(is_null($this->creado)) {
            $this->creado = new \DateTime();
        }
        
        $this->actualizado = new \DateTime();

        if(!$this->seudonimo) {
            $this->seudonimo = '';
        }
    }
}
