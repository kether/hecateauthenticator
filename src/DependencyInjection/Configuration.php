<?php 

namespace EstudioHecate\Bundle\HecateAuthenticator\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('hecate_authenticator');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('hecate_authenticator')
                    ->children()
                        ->scalarNode('username_field')->end()
                        ->scalarNode('password_field')->end()
                        ->scalarNode('remember_field')->end()
                        ->scalarNode('token_field')->end()
                    ->end()
                ->end() // hecate_authenticator
            ->end()
        ;

        return $treeBuilder;
    }
}