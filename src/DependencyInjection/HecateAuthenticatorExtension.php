<?php 

namespace EstudioHecate\Bundle\HecateAuthenticator\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class HecateAuthenticatorExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        
        $configs = $this->processConfiguration($configuration, $configs);
    }
}
