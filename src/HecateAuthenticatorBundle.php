<?php

namespace EstudioHecate\Bundle\HecateAuthenticator;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HecateAuthenticatorBundle extends Bundle
{
    const VERSION = '2.0';
}